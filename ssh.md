## 操作演示
### 查看本地是否配置公私密钥
我们直接开始将如何进行配置，在配置前我们要先看一下我们本地是否已经配置公私密钥：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222173035366.png)
执行上图命令：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190223104922439.png)
如上图所示：表示我本地没有有密钥，或者有.ssh目录但是没有id_rsa和id_rsa.pub文件

> 如果有id_rsa和id_rsa.pub文件则无需再次生成公钥和私钥 直接进行公钥配置即可。
### 生成公私密钥

执行 **ssh-keygen -t rsa -b 4096 -C "your_email@example.com"**

> ssh-keygen 命令不受目录限制 可以在任意目录下执行

输入命令后一律点击回车 直至显示如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222170104414.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xqazEyNnd5,size_16,color_FFFFFF,t_70)

上图中会说明生成的公钥和私钥生成的路径如下图中标红的位置

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222173529446.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xqazEyNnd5,size_16,color_FFFFFF,t_70)

**id_rsa :私钥**
**id_rsa.pub:公钥**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222170208462.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xqazEyNnd5,size_16,color_FFFFFF,t_70)

### 在github上配置我们的公钥内容
登录我们的github点击如下图中Settings 标签。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222170311213.png)

然后再点击 **SSH and GPG keys**

![在这里插入图片描述](https://img-blog.csdnimg.cn/2019072516524132.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

点击 New SSH key 添加我们的公钥的内容

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222170422187.png)

这里的title可以不输入信息。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222170506323.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xqazEyNnd5,size_16,color_FFFFFF,t_70)

如下图所示表示添加成功

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222173946542.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xqazEyNnd5,size_16,color_FFFFFF,t_70)

然后复制我们ssh 的克隆链接 执行 **git clone 克隆链接url** 如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222170731636.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xqazEyNnd5,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190222170647631.png)
## 参考文献
https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent