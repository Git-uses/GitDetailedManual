官方git help 介绍如下下图：[点击访问查看](https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E8%8E%B7%E5%8F%96%E5%B8%AE%E5%8A%A9)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323172115288.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)
具体操作方式如下：
首先通过点击 **Git Bash Here** 进行我们操作命令的cmd 界面

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323172903105.png)

执行 **git help commit** 然后点击回车 会通过游览器打开一个命令帮助文档。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323172950299.png)

如下图所示就是红框中 -m 命令的介绍 

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323173026157.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

如果你只是想在命令窗口进行查看  可以执行 **git commit -h** 即可

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323194146637.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)