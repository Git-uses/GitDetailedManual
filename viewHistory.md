[Git官方查看提交历史记录操作文档](https://git-scm.com/book/zh/v2/Git-%E5%9F%BA%E7%A1%80-%E6%9F%A5%E7%9C%8B%E6%8F%90%E4%BA%A4%E5%8E%86%E5%8F%B2)   部分截图内容如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/2019021720051156.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xqazEyNnd5,size_16,color_FFFFFF,t_70)
我们也可以通过 **git help --web --log** 查看详细的命令。

我这里主要介绍一些比较常用的
- **git log** 
查看当前分支所有提交历史记录
- **git log -p -2**
最近两次提交差异显示
-  **git log --pretty=oneline** 
 查看简洁提交历史记录
-  **git log --oneline**
 查看简洁提交历史记录
-  **git log --graph**
 显示ASCII 图形历史记录
-  **git log --all**
 查看所有分支的提交历史记录
-  **git log --all --graph**
查看所有分支的提交 ASCII 图形历史记录
- **git log -n2**
查看最近2次提交的历史记录
- **git reflog**  
查看所有分支的所有操作记录 （包括被删除的commit 操作记录）一般用于 git reset --hard 切换使用。