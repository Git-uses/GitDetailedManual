## Git 图形界面的操作方式
我们可以通过 **gitk** 和 **git-gui**  2种方式进行图形化界面的操作。官网介绍比较笼统，我这里通过详细操作让您快速上手 git 图形界面。
## gitk
进入我们的git仓库然后执行gitk即可 具体操作方式如下：
在git仓库目录右键点击**Git Bash here**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323162310155.png)

输入 **gitk** 然后点击回车

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323162137213.png)

> 我的提交记录如下 1 创建一个a.txt 文件并提交 注释是 add a.txt 2 在a.txt 中添加 a....内容 注释是 add
> content a....

如下图所示就是我们gitk的图形化界面。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323163621368.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323163824815.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323163859210.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)
## git-gui
在git 仓库目录下 点击**Git GUI Here** 具体的操作方式如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323164045858.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

如下图所示就是操作界面具体功能区域。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323165346664.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

> 文件内容 包括文件内容和新增 修改的的内容都会进行显示。

点击红色框的图标可以是文件在工作区和暂存区进行切换。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323165931152.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

如下图所示 红色框标注的五个按钮具体操作表示如下：
**rescan**:刷新，**stage changed:** 提交交到暂存区，**sign off:**检查完成，**commit:**提交到本地仓库，**push:**上传到远程服务器。
我们在操作时依次点击即可将文件由 工作区 -> 暂存区->本地仓库->远程仓库 的上传操作。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323170047779.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323171324156.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

查看提交历史记录和执行 gitk 效果一致。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323171345443.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

关于git 图形化界面官方给出相关的介绍。具体介绍请访问 [A1.1 附录 A: 其它环境中的 Git - 图形界面](https://git-scm.com/book/zh/v2/%E9%99%84%E5%BD%95-A:-%E5%85%B6%E5%AE%83%E7%8E%AF%E5%A2%83%E4%B8%AD%E7%9A%84-Git-%E5%9B%BE%E5%BD%A2%E7%95%8C%E9%9D%A2)