## 三个撤销操作命令
关于git 的撤销操主要有以下三个：

 **1. git chekout --文件名称 未提交到缓存区的代码进行撤销**
 **2. git reset HEAD 文件名称  将暂存区的代码进行撤销到工作区中**
 **3. git reset --hard 要切换commit 的记录id  commit 版本回退** 
   > git reset --hard 命令比较危险 可能导致工作目录中所有当前进度丢失！
   
 # git chekout 操作演示
 如下图所示我们的git仓库中本地仓库有 a.txt b.txt c.txt 三个文件。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323180147415.png)

 我们修改b.txt 文件的内容如下：

 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323180745968.png)

 通过我们的GUI 进行查看如下图所示：![在这里插入图片描述](https://img-blog.csdnimg.cn/201903231808499.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)
 然后执行 **git checkout -- b.txt**

 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323181000602.png)
 
如下图所示我们的文件恢复为原来的内容。

![!(https://img-blog.csdnimg.cn/2019032318105941.png)](https://img-blog.csdnimg.cn/2019032318111561.png)![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323181022919.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)
 

> 需要注意的是git chekout 撤销操作前提是必须执行过git add 后的文件才会管用。

  
## git reset HEAD 操作演示
新建d.txt 并将其提交到暂存区中。具体操作如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323181256199.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

执行 **git reset HEAD -- d.txt** 如下图所示 d.txt 恢复到了工作区。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323181356343.png)
## git reset --hard 操作演示
在执行 git reset --hard 命令之前 我们先查看一下历史提交记录 最新的记录为 add c.txt

![在这里插入图片描述](https://img-blog.csdnimg.cn/2019032318173198.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

我们将上面的d.txt 提交到 本地仓库中。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323181800127.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323181819961.png)

查看历史提交记录 我们最新记录为 add d.txt

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323181837691.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

执行 **git reset --hard 要切换commit 的记录id** 我们这里将commit 切换到  add c.txt 的commit 上。执行完成后如下图所示我们最新提交记录恢复到了 add c.txt 的commit 上。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323182342786.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

我们可以通过 **git refog** 查看之前的提交历史记录。在次执行 git reset --hard 要切回的commit id  具体操作如下图所示：![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323182519749.png)![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323182621435.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323182635480.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

参考文献 [官网介绍 2.4 Git 基础 - 撤消操作](https://git-scm.com/book/zh/v2/Git-%E5%9F%BA%E7%A1%80-%E6%92%A4%E6%B6%88%E6%93%8D%E4%BD%9C)