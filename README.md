# GitDetailedManual

## 介绍
GitDetailedManual（Git 详细手册） 旨在通过详细的操作介绍，带你快速了解Git如何使用。 并通过Git打开开源的大门

## 操作教程
- [Git 介绍](https://gitee.com/Git-uses/GitDetailedManual/blob/master/introduce.md)
- [Git 官方安装教程（Windows版）](https://gitee.com/Git-uses/GitDetailedManual/blob/master/gitInstall.md)
- [Git 使用初体验](https://gitee.com/Git-uses/GitDetailedManual/blob/master/usingFirstExperience.md)
- [Git 批处理操作](https://gitee.com/Git-uses/GitDetailedManual/blob/master/batchProcessing.md)
- [Git 查看提交历史记录](https://gitee.com/Git-uses/GitDetailedManual/blob/master/viewHistory.md)
- [Git 历史内容对比](https://gitee.com/Git-uses/GitDetailedManual/blob/master/compare.md)
- [Git 撤销相关操作](https://gitee.com/Git-uses/GitDetailedManual/blob/master/reset.md)
- [Git 查看帮助命令](https://gitee.com/Git-uses/GitDetailedManual/blob/master/viewHelpCommand.md)
- [Git HTTP方式克隆远程仓库到本地](https://gitee.com/Git-uses/GitDetailedManual/blob/master/http.md)
- [Git SSH方式克隆远程仓库到本地](https://gitee.com/Git-uses/GitDetailedManual/blob/master/ssh.md)
- [Git 分支相关操作](https://gitee.com/Git-uses/GitDetailedManual/blob/master/branch.md)
- [Git tag(标签)相关操作](https://gitee.com/Git-uses/GitDetailedManual/blob/master/tag.md)
- [Git 图形化界面操作](https://gitee.com/Git-uses/GitDetailedManual/blob/master/graphicInterface.md)

## 付费操作教程

自己在GitChat上写了2篇 关于Git操作的收费图文教程，相对于上面的教程更深入和详细介绍 Git 的相关操作。课程内容介绍如下：

### 手把手带你玩转 Git

目前国内开发人员普遍都开始使用 Git 进行项目的版本管理，如果说你还在使用 SVN 并且对 Git 一无所知的话，那我的这个 Chat 将会帮助到你。

我将手把手教你入门，并且巡回渐进的带入到工作的实际应用中。

在本场 Chat 分享内容如下：

1. Git 由来和 Git 的作用以及 Git 工作流程；
1. Window 环境和 Linux 环境下安装 Git；
1. 带你玩转 Github 和 码云（Gitee）的操作；
1. 快速掌握 Git 配置以及 Git 命令的基本操作；
1. 快速掌握分支的创建切换合并以及解决代码冲突；
1. Git Rebase 和 Git Merge 区别以及 Git Rebase 使用注意事项；
1. Eclipse 使用 Git 常用操作以及注意事项；
1. IntellJ IDEA 使用 Git 常用操作以及注意事项。


### Git 实用操作手册

首先声明一下该篇 Chat 适合了解并使用过 Git 的同学，之前我在 GithChat 上分享了一篇《手把手带你玩转 Git》上面讲的内容比较偏基础一些，非常适合完全没有接触过 Git 的同学。

这篇 Chat 更为系统地介绍了，如何通过纯命令方式来操作 Git。全篇教程是在 Windows 系统 上操作的（使用 Mac 的同学也没有关系，因为命令操作方式都是一样的）。通过先介绍命令作用 ，然后是大量图片操作演示来进行讲解。希望通过我的 Chat 带你领悟 Git 的魅力并爱上使用 Git。

通过这篇 Chat 你将学习到以下内容：


1. 基础命令以及拓展操作
1. 历史记录内容的对比
1. 远程仓库相关操作：包括 HTTPS 和 SSH 的方式克隆
1. Git 本地以及远程仓库分支 CRUD
1. 分支合并操作以及冲突解决
1. Git 本地以及远程仓库标签 CRUD
1. 图形化界面查看 Git 仓库
1. Git 高级操作
1. Git 需要谨慎使用的危险命令
1. Git 原理的介绍

通过微信可以查看课程的详细信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/0726/133313_917d7ac6_729641.jpeg "mmexport1564117173865.jpeg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0726/133322_589d19c3_729641.jpeg "TIM图片20190726133130.jpg")

