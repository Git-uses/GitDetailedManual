## 前言
通过该文你将了解到 Git 使用操作的一些最为基础的命令，带你快速体验一下 Git 的基础操作。
## Git 基础命令操作介绍
### 配置用户信息
用户信息配置是通过 **git config** 命令来进行操作的，我们可以通过下面两个命令来进行配置用户信息（代码提交者）：
**git config --global user.name '用户名称'**
**git config --global user.email '有效邮箱'**

通过 **git config --list --global** 查看配置信息，如下图所示：

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164955_9d4da2f8_729641.png)

> Windows 系统用户可以右击菜单，选择 Git Bash Here 打开Git 命令行界面 
>
> ![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164955_75aea9ed_729641.png)

### 创建 Git 仓库
通过 **git init** 命令创建 Git 仓库 具体操作如下：

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164955_cff2d727_729641.png)

在demo目录下有如下图git 隐藏文件夹表示创建成功！

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164956_c8c955db_729641.png)

### 将工作区代码放入暂存区
在demo 目录下创建 a.txt 文件具体内容如下：

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164955_b6a43878_729641.png)

**git status** 查看本地仓库状态，如下图所示表示**工作区**有一个 a.txt 文件

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164956_f6ff0aa8_729641.png)

我们可以通过 git add 文件名称 将文件由**工作区**放入到**暂存区**中。

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164957_6ddab29e_729641.png)

如下图所示表示文件已经成功放入**暂存区**中。

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164959_31ac970e_729641.png)

### 将暂存区的代码放入本地仓库
我们可以通过 **git commit -m '提交注释'** 来将**暂存区**的代码放入**本地仓库**中。

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164955_d6dc38c7_729641.png)
### 查看提交历史
我们可以通过 **git log** 查看提交历史

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164956_d59adcb3_729641.png)
### 修改文件名称
将a.txt 文件修改成 ab.txt 执行**git status** 内容如下：

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164956_5352bf76_729641.png)

通过 先执行 **git rm a.txt** 然后在执行 **git add ab.txt** 操作修改文件的名称
当然最终还是要执行一次commit的。

也可以不操作a.txt文件 直接通过 **git mv a.txt ab.txt** 进行文件的名称修改。

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/164958_5c244f69_729641.png)

## 小结
 Git 基础命令操作中使用到的命令如下：

 - **git config --global user.name '用户名称'**： 配置提交者 用户名称
 - **git config --global user.email '有效邮箱'** ：配置提交者 有效邮箱
 - **git config --list --global**：查看配置信息
 - **git init**:创建 Git 仓库
 - **git status**:查看本地仓库状态
 - **git log**:查看提交历史
 - **git rm 文件名**:将文件从本地仓库删除
 - **git add 文件名**:将文件添加到暂存区中
 -  **git commit -m '提交注释'**：将暂存区的文件提交到本地仓库
 - **git mv 原文件名称 新文件名称**:修改本地仓库中文件的名称
 
 需要注意的是 **git config --global user.email** 配置的邮箱一定要是有效的邮箱。