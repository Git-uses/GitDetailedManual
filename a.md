# 1 git简介


## 1.1 git的由来

> 在介绍**git**之前我们来先说一下**git**是怎么来的，**git**的创始人同时也是
> **Linux**的创始人*Linus*。*Linus*早期在维护**Liunx**代码一直是人肉合并代码。后期开始采用了分布式版本控制系统**BitKeeper** 来进行维护。然而**BitKeeper** 由于考虑商业的原因，不在免费提供给**Linux**使用，后来
> *Linus*就自己开发一个。于是乎就有了后来的**git**。

##1.2 git的作用

> **git**的作用分为3块：**管理项目的版本** ，**多人协同开发一个项目**，**分布式保存项目。** git不像svn一样，他可以没有中央仓库服务器，每个开发人员都有一个完整的仓库，在开发完毕后在进行互相push。当然对于多个人的团队开发git也是有远程仓库方便开发人员快速修改合并代码的。

##1.3 git的工作流程

**git** 的工作区域有4个分别是：**远程仓库**，**本地仓库**，**工作区** ，**暂存区**。 执行操作如下： 首先我们先从远程仓库 克隆（clone）到本地仓库，然后在工作区中添加或者修改文件。 提交文件到本地仓库，我们需要先通过add命令将文件放入暂存区中，然后在通过commit命令将文件放入到本地仓库中。我们通过push命令将文件从本地仓库推送到远程仓库中。更新别人提交的代码需要我们通过pull命令将代码更新到工作区。

![enter image description here](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9pbWFnZXMuZ2l0Ym9vay5jbi9iMWY4MTZkMC1lYTFjLTExZTgtYjc0ZC1mNzcwYWM5ODNhYTU)
