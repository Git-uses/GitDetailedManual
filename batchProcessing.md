## 前言
我们可以通过 git add 文件1 文件2 文件3 将多个文件一次性提交到暂存区中，如果你有100个甚至是1000个呢？通过该文你将了解到如何通过Git 提供的快速操作文件由工作区到暂存区的三个命令，让你快速执行批处理操作。另外 Git 还提供了可以将修改后的文件直接由工作区提交到本地仓库中。
### git add .
执行该命令后会将修改和新增的文件，直接提交到暂存区中。

**示例演示：**
如下图所示 添加 a.txt b.txt c.txt 三个文件

![图片: https://uploader.shimo.im/f/gmJLoMVbQIkJRpVp.png](https://images.gitee.com/uploads/images/2019/0724/152753_151178b3_729641.png)

通过git status 查看三个文件，如下图所示 表示是三个文件目前处在工作区中。

![图片: https://uploader.shimo.im/f/L1a07Uf5Y78v2egj.png](https://images.gitee.com/uploads/images/2019/0724/152752_80858c4a_729641.png)

执行 git add .

![图片: https://uploader.shimo.im/f/772W61TTxngaclJp.png](https://images.gitee.com/uploads/images/2019/0724/152751_4f3ed928_729641.png)

如下图所示：我们成功将三个文件由工作区提交到了 暂存区中。

![图片: https://uploader.shimo.im/f/XPbZih459Qke9eV0.png](https://images.gitee.com/uploads/images/2019/0724/152751_bd3bdb22_729641.png)

### git add -u
该命令仅将修改后的文件放入暂存区中，不会提交新增的文件。

**示例演示：**
如下图所示 修改 a.txt 并创建一个新文件 d.txt

![图片: https://uploader.shimo.im/f/rPnlAfsZriwtwHrJ.png](https://images.gitee.com/uploads/images/2019/0724/152751_d1217c17_729641.png)

然后执行 git add -u

![图片: https://uploader.shimo.im/f/fdCOOeOl92gUvsNX.png](https://images.gitee.com/uploads/images/2019/0724/152751_60eff4d7_729641.png)

通过 git status 我们成功将 a.txt 提交到暂存区中，但是 d.txt 并没有体交到暂存区中。

![图片: https://uploader.shimo.im/f/KjDQ6giRMnwrNqGT.png](https://img-blog.csdnimg.cn/20190330121737576.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

### git add -a
**git add -a** == **git add . + git add -u** 该命令就是上面2个命令介绍的组合。

## 一步操作完成由工作区到本地仓库
### git commit -am '注释内容'
该命令仅将修改后的文件直接提交到本地仓库中，相当于执行 git add -u + git commit -m '注释内容'

**示例演示：**

添加 d.txt 文件 内容为 d...

![图片: https://uploader.shimo.im/f/sesd8Qiu5Wgu2wI7.png](https://images.gitee.com/uploads/images/2019/0724/152752_5bb1062e_729641.png)

通过 **git add** 命令将其添加到暂存区。

![图片: https://uploader.shimo.im/f/J1LWFCbg6noJ7ayK.png](https://images.gitee.com/uploads/images/2019/0724/152755_7db9beca_729641.png)

修改d.txt 内容为 111 d... 具体修改就不做演示了，

查看d.txt 的状态如下图所示，如果我们想将修改内容提交到本地仓库，我们就要使用 git add d.txt + git commit d.txt -m'添加注释信息' 来完成。

![图片: https://uploader.shimo.im/f/LTh3LIBNb1YXrXGL.png](https://images.gitee.com/uploads/images/2019/0724/152754_d1b88d47_729641.png)

直接执行 git commit -am '注释信息' 就可以将d.txt 直接提交到本地仓库了。

![图片: https://uploader.shimo.im/f/jRGfnRqqQmwmgZX4.png](https://images.gitee.com/uploads/images/2019/0724/152754_ee982569_729641.png)

我们通过git log进行查看 我们修改的 d.txt 确实提交到了本地仓库中。

![图片: https://uploader.shimo.im/f/pDGfUwM8V0YcmcXP.png](https://images.gitee.com/uploads/images/2019/0724/152754_18936201_729641.png)

## 小结
如果出现大量的新增或修改文件提交到暂存区，可以使用 **git add .** 、**git add -u** 、**git add -a** 进行操作。

 - **git add .：**   修改和新增的文件
 - **git add -u：**  修改的文件不包含新增的文件
 - **git add -a：**  git add .+git add -u
对于修改后的文件可以直接通过 **git commit -am '注释内容'** 将其直接提交到本地仓库中。通过这些批处理操作将帮你大大提高操作的效率，希望看完后在工作中抓紧使用起来。