## 操作步骤
Git 通过HTTP方式克隆远程仓库到本地总共需要三步：

 1. 登录我们的github 复制我们的github 项目的https 地址。
 2. 在我们的本地磁盘下创建文件夹用来保存我们的远程git项目。
 3. 通过**git clone 远程仓库地址** 克隆我们远程仓库的项目

## 操作演示
第一步 登录我们的github 复制我们的github 项目的https 地址
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323202747386.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

第二步在本地磁盘下创建文件夹用来保存我们的远程git 项目

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323203100609.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

第三步 通过**git clone 远程仓库地址** 克隆我们远程仓库的项目

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323203227710.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190323203259138.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)
