## 前言
在工作中我们经常会遇到要查看不同历史版本内容对比、工作区和暂存区的对比、暂存区和本地仓库内容对比。本文将一步步带你了解如何进行上述操作。
## 工作区和暂存区内容的比较
### git diff -- 文件名称
如下图所示 我们的first.txt （原来文件内容为空） 文件处于工作区 修改内容为 first 执行 git diff -- first.txt 后会考到内容新增加啦 first的内容。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190330120128932.png)
## 暂存区和最新提交内容比较
### git diff --cached
如下图所示我们在将已经体交到暂存区的 first.txt（原来文件内容为空） 进行修改 内容为 first 然后执行git diff --cached 后的效果。  

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190330120112836.png)

## 查看不同历史提交文件内容的不同
**git diff 比较commitId1 比较commitId2 -- 比较的文件**
使用git log 查看我们first.txt 文件 的历史记录如下图所示： 

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190330120147304.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190330120157184.png)

执行如下图命令 对比 17a5 和 d048 开头2个提交记录 的first.txt 文件内容有什么不同。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190330120211569.png)