## 前言
通过该文你将了解到如何 Git 本地仓库以及远程仓库 tag 标签的增删改查操作。
## 什么是标签？
Git 的标签我们可以理解成一个项目管理中的里程碑，在开发中我们可以通过标签将一些写好的功能做一个标记。
## 标签的增删改查
### 1.创建标签
在 Git 中标签分为2种 
 **1.轻量标签（lightweight）**
	轻量标签 一般是用于临时的标签 ，轻量标签仅仅记录了commit的信息
 **2. 附注标签（annotated）**
 附注标签 记录的信息更为详细 它包含了创建标签的作者 创建日期 以及标签信息。一般建议创建附注标签。
  #### 1.1 创建 附注标签
  **git tag -a 标签名称 -m '标签注释信息'**

 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326145448950.png)

 #### 1.2 创建 轻量标签
  **git tag  标签名称**  

 ![在这里插入图片描述](https://img-blog.csdnimg.cn/2019032614575920.png)

  #### 1.3 通过指定commitId 创建
 **git tag -a commitId**
 假如你想对以前的commit 创建标签 我们只需要在 标签名称后面指定 commit id即可，具体操作如下图所示：
 
 通过git log 查看历史提交记录

 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326160227563.png)

我们在commit id b3bba31 的commit 上进行创建标签。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326160211209.png)

如下图所示我们的标签创建成功！

![在这里插入图片描述](https://img-blog.csdnimg.cn/2019032616024547.png)

### 2 查看标签
**git tag** 查看说有的标签

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326150905543.png)

 **git tag -l '标签名称'**  查询指定标签名称的列表

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326151047802.png)

**git tag -l '名称.*'** 模糊查询标签列表。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2019032615165299.png)

**git show 标签名称** 查询标签的具体信息
附注标签的信息如下图所示

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326151146120.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

轻量标签 信息如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326151230649.png)

### 3 删除标签
**git tag -d 标签名称**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326151928658.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

### 4 将标签提交到远程仓库

**git push test 标签名称**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326154138509.png)

> 需要注意：一般情况下是 git push origin 标签名称 我本地定义别名是test 在操作前请执行如下图命令查看链接远程仓库的别名
>
> ![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326154301397.png)
> 
如下图所示 我们的v1.0标签成功提交到了远程仓库中。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326154501601.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)

如果你觉得一个个提交标签比较麻烦，我们也可以通过 **git push test --tags** 进行批量提交。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326154640461.png)

### 5 删除标签
**git tag -d 标签名称**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326154833280.png)

 我们删除本地仓库的标签后，远程仓库的标签并不会进行删除。
 
 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326154907767.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)
 
 我们需要执行 **git push test :refs/tags/v1.1** 将远程仓库的标签进行删除。
> ![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326155039960.png)
如下图所示我们的标签删除成功！

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190326155125568.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly96aHVvcWlhbm1pbmd5dWUuYmxvZy5jc2RuLm5ldA==,size_16,color_FFFFFF,t_70)
## 小结
标签是 Git 最大特点之一，通过标签可以轻松的定义项目版本、里程碑。在实际工作中也是使用较为频繁的功能。 虽然 Git 可以创建轻量标签，但是工作中建议创建附注标签。
## 参考文献
 [2.6 Git 基础 - 打标签](https://git-scm.com/book/zh/v2/Git-%E5%9F%BA%E7%A1%80-%E6%89%93%E6%A0%87%E7%AD%BE)
