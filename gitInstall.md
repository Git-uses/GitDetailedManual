## 前言
Git是一个免费的开源 分布式版本控制系统，旨在快速高效地处理从小型到大型项目的所有事务。

Git 易于学习， 占地面积小，具有闪电般快速的性能。它超越了Subversion，CVS，Perforce和ClearCase等SCM工具，具有廉价本地分支，便捷的临时区域和 多个工作流程等功能。
---已上内容介绍摘抄 Git 官网介绍。

看完官网的介绍，你是不是有想试一试 Git 的冲动呢？如果有那么你需要先安装 Git 到你使用的系统中。接下来就开始Git安装之旅吧！

> 该文主要介绍了如何在Windows 上安装 Git，关于Linux和其他系统可以参考官网教程介绍

## 安装图文教示例
访问 git [官网](https://git-scm.com/) 内容如下图：
![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/161057_7f94a598_729641.png)
第一步先 点击 **Documentation**  查看相关文档信息，具体内容如下图：

 - Reference 是参考手册
 - Book 是具体的介绍
![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/161056_6d0f1285_729641.png)
点击**Pro Git book** 如下图红色框位置：
![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/161057_a0bf1c98_729641.png)

> Pro Git book 具体链接：https://git-scm.com/book/en/v2

**Pro Git book** 具体内容如下：

![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/161056_67f1f903_729641.png)
我们可以根据自己的喜好选择对应的语言版本：我们这里选择简体中文，选择后点击 **安装 Git**
![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/161057_8003b1ed_729641.png)
![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/161056_e1cf831f_729641.png)我自己平常使用的是 Windows 系统，我这里就操作一下如何在Windows上安装git
第一步先查看官网Windows安装教程：
![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0722/161056_c3a805b7_729641.png)
访问 https://git-scm.com/download/win 下载window 安装包

![enter image description here](https://images.gitee.com/uploads/images/2019/0722/161057_9aac93af_729641.png)

建议下载安装版 进行傻瓜式的下一步下一步即可。安装成功后 点击桌面鼠标右击显示2个 Git菜单表示安装成功。 如下图：

![enter image description here](https://images.gitee.com/uploads/images/2019/0722/161057_c4b2860b_729641.png)
## 小结
Git 安装还是比较简单的，Windows 系统建议使用安装版，关于其他的系统的教程Git官网也有详细的介绍可以访问 [Git 官方中文安装教程](https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git) 进行查看